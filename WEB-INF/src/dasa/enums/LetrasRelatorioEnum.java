package dasa.enums;

public enum LetrasRelatorioEnum {
	A(40), Ar(41),
	P(30),Pe(32),Pf(33),Pr(31),
	T(11),Ts(12),Tt(13),Tf(14),Td(15),
	M(20),Me(22),Mf(23),Mo(24),Mr(21),
	N(1),MoX(24);
	
	public int valorPonto;
	
	LetrasRelatorioEnum(int valor) {
		valorPonto = valor;
	}
}
