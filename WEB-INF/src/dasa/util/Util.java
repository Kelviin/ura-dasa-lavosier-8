package dasa.util;

import org.apache.axis.utils.StringUtils;

import br.com.coddera.receptivo.dasa.util.RequestValues;
import dasa.enums.LetrasRelatorioEnum;

public class Util {
	public static String concatenaValoresRelatorio(String ponto) {
		String pontoFinal = "";
		String letras = "";
		int marcadorPonto = 0;
		int marcadorFim = ponto.length();

		char[] c = ponto.toCharArray();
		
		loop:{
			for (char v : c) {
				if (!Character.isDigit(v)){
					if (v == '_'){
						marcadorFim = ponto.indexOf(v);
						break loop;
					}
					letras += v;
					marcadorPonto++;
				}
			}
		}

		try{
			int valorLetra = LetrasRelatorioEnum.valueOf(letras).valorPonto;
			pontoFinal = valorLetra + ponto.substring(marcadorPonto, marcadorFim);
		} catch (IllegalStateException e) {
			e.printStackTrace();
			throw e;
		}
		return pontoFinal;
	}
	
	public static String concatenaValoresOpcaoRelatorio(String ponto, int opcao){
		String pontoFinal = "";
		String letras = "";
		int marcadorPonto = 0;
		int marcadorFim = ponto.length();

		ponto = ponto.replace('X', (char) (opcao + '0'));
		
		char[] c = ponto.toCharArray();
		
		loop:{
			for (char v : c) {
				if (!Character.isDigit(v)){
					if (v == 'X')
						letras += opcao;
					if (v == '_'){
						marcadorFim = ponto.indexOf(v);
						break loop;
					}
					letras += v;
					marcadorPonto++;
				}
			}
		}

		try{
			int valorLetra = LetrasRelatorioEnum.valueOf(letras).valorPonto;
			pontoFinal = valorLetra + ponto.substring(marcadorPonto, marcadorFim);
		} catch (IllegalStateException e) {
			e.printStackTrace();	
			throw e;
		}
		return pontoFinal;
	}
	
	public static int verificaRetornoErro(Throwable throwable){
		if (throwable.getMessage().toLowerCase().contains("timed out")){
			return 1;
		} else {
			return 2;
		}
	}
	
	public static String siglaMarca(String marca){
		String sigla = "";
		if("Delboni".equalsIgnoreCase(marca)){
			sigla = RequestValues.DELBONI_AURIEMO.getString();
		}else if("Bronstein".equalsIgnoreCase(marca)){
			sigla = RequestValues.BRONSTEIN.getString();
		}else if("Lavoisier".equalsIgnoreCase(marca)){
			sigla = RequestValues.LAVOISIER.getString();
		}else if ("SergioFranco".equalsIgnoreCase(marca)){
			sigla= RequestValues.SERGIO_FRANCO.getString();
		}else if("CDPI".equalsIgnoreCase(marca)){
			sigla = RequestValues.CDPI.getString();
		}else if("AltaConcierge".equalsIgnoreCase(marca)){
			sigla = RequestValues.ALTA_CONCIERGE.getString();
		}else if("AltaConciergeSP".equalsIgnoreCase(marca)){
			sigla = RequestValues.ALTA_CONCIERGE.getString();
		}else if("AltaConciergeRio".equalsIgnoreCase(marca)){
			sigla = RequestValues.ALTA_CONCIERGE.getString();			
		}else{
			sigla = "";
		}
		
		return sigla;
	}
	
	public static String retornaPontoTransacao (String ponto, int tipoErro){
		char letraRetorno = ' ';
		switch (tipoErro) {
		case 1:
			letraRetorno = 'f';
			break;
		case 2:
			letraRetorno = 't';
			break;
		default:
			letraRetorno = 's';
			break;
		}
		return ponto.replace('x', letraRetorno);
		
	}
	
	public static String verificaANIComLetra(String ani){
		char[] c = ani.toCharArray();
		loop:{
			for (char d : c) {
				if (!Character.isDigit(d)){
					ani = "0000000000";
					break loop;
				}
			}
		}
		
		return ani;
	}
	
	public static String retornoMes(int valor){
		String mes = "";
		switch (valor) {
		case 1:
			mes = "janeiro";
			break;
		case 2:
			mes = "fevereiro";
			break;
		case 3:
			mes = "marco";
			break;
		case 4:
			mes = "abril";
			break;
		case 5:
			mes = "maio";
			break;
		case 6:
			mes = "junho";
			break;
		case 7:
			mes = "julho";
			break;
		case 8:
			mes = "agosto";
			break;
		case 9:
			mes = "setembro";
			break;
		case 10:
			mes = "outubro";
			break;
		case 11:
			mes = "novembro";
			break;
		case 12:
			mes = "dezembro";
			break;

		default:
			break;
		}
		
		return mes;
	}
	
	public static String retornaDataAVerbalizar (String dataMacacao){
		String dataValidacao = "";
		if(dataMacacao.length() > 5){
			String split[] = StringUtils.split(dataMacacao, '/');
			for (int i = (split.length-1); i > -1; i--) {
				dataValidacao += split[i];
			}
		}
		
		return dataValidacao;
	}
	
	
	public static String retornaValorPontoRetencao (String pontoMarcacao){
		String pontoRetencao = "";
		char primeiraLetra = pontoMarcacao.charAt(0);
		char segundaLetra = pontoMarcacao.charAt(1);
		String letras = "";
		int marcadorPonto = 0;
		int marcadorFim = pontoMarcacao.length();
		int pontoFinal;
		
		char[] c = pontoMarcacao.toCharArray();
		
		for (char v : c) {
			if (!Character.isDigit(v)){
				if(v == '_'){
					marcadorFim = pontoMarcacao.indexOf(v);
					break;
				}
				letras += v;
				marcadorPonto++;
			}
		}
		
		if(marcadorPonto <= 2){
			switch (primeiraLetra) {
			case 'A':
				letras = "Ar";
				break;
			case 'P':
				letras = "Pr";
				break;
			case 'M':
				letras = "Mr";
				if(segundaLetra == 'o'){
					pontoMarcacao = letras + pontoMarcacao.substring(marcadorPonto+1);
					marcadorFim--;
				}
				break;
			}
		} else {
			letras = "Mr";
			pontoMarcacao = letras + pontoMarcacao.substring(marcadorPonto+1);
			marcadorPonto--;
			marcadorFim--;
			marcadorFim--;
		}
		
		try{
			int valorLetra = LetrasRelatorioEnum.valueOf(letras).valorPonto;
			pontoRetencao = valorLetra + pontoMarcacao.substring(marcadorPonto, marcadorFim);
		} catch (IllegalStateException e) {
			e.printStackTrace();
			throw e;
		}
		return pontoRetencao;
	}
	
	public static String retornaMarcacaoFinaliza (String pontoMarcacao){
		String pontoFinal = "";
		int marcadorPonto = 0;
		int somador = 0;

		char chPrimeiraLetra = pontoMarcacao.charAt(0);
		String primeiraLetra = "" + chPrimeiraLetra;
		
		char[] c = pontoMarcacao.toCharArray();

		for (char v : c) {
			if (!Character.isDigit(v)){
				marcadorPonto++;
			}
		}

		switch (chPrimeiraLetra) {
		case 'M':
			somador = 5;
			break;
		case 'P':
			somador = 4;
			break;
			
		}
		
		try{
			pontoFinal += (LetrasRelatorioEnum.valueOf(primeiraLetra).valorPonto + somador) 
					+ pontoMarcacao.substring(marcadorPonto);
		}catch(Exception e){
			e.printStackTrace();
		}
		return pontoFinal;
	}
}
