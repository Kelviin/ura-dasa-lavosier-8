package dasa.util;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

import com.avaya.sce.runtimecommon.ITraceInfo;

public class Propriedade {
	
	public enum EnumPropriedade {
		VDN_SP,VDN_RJ,CONFIG;
	}

	//private static final String CONFIG = "config.properties";
	private static final String VDN_SP = "vdn_Lavoisier_SP.properties";
	private static final String VDN_RJ = "vdn_Lavoisier_RJ.properties";
	private static final String CONFIG = "config.properties";

	private Properties props;
	private String arquivo;

	/**
	 * 
	 * @param tipo - tipo do arquivo de configuracao
	 * @see EnumPropriedade
	 */

	public Propriedade(EnumPropriedade tipo, com.avaya.sce.runtimecommon.SCESession mySession) {
		FileInputStream fis = null;
		String path = System.getProperty("catalina.home")+"/conf/lavoisier/";
		//String path = "/opt/AppServer/apache-tomcat-7.0.70/conf/tokio_marine/";
		ITraceInfo logger = mySession.getTraceOutput();
		switch (tipo) {

		//VDN SP	
		case VDN_SP:
			arquivo = VDN_SP;
			try {
				fis = new FileInputStream(path + arquivo);
				props = new Properties();
				props.load(fis);
				logger.writeln(ITraceInfo.TRACE_LEVEL_INFO, "######################### SP ##############################");
				logger.writeln(ITraceInfo.TRACE_LEVEL_INFO, "## CAMINHO: "+path);
				logger.writeln(ITraceInfo.TRACE_LEVEL_INFO, "## NOME DO ARQUIVO: "+arquivo);
				logger.writeln(ITraceInfo.TRACE_LEVEL_INFO, "###########################################################");
			} catch (IOException e) {
				logger.writeln(ITraceInfo.TRACE_LEVEL_INFO, "###########################################################");
				logger.writeln(ITraceInfo.TRACE_LEVEL_INFO, "ERRO:");
				e.printStackTrace();
				logger.writeln(ITraceInfo.TRACE_LEVEL_INFO, "###########################################################");
			}finally {
				try {
					if (fis != null) {
						fis.close();
					}
				} catch (IOException e) {
					e.getMessage();
				}
			}
			break;
		
		//VDN RIO	
		case VDN_RJ:
			arquivo = VDN_RJ;
			try {
				fis = new FileInputStream(path + arquivo);
				props = new Properties();
				props.load(fis);
				logger.writeln(ITraceInfo.TRACE_LEVEL_INFO, "######################### RJ ##############################");
				logger.writeln(ITraceInfo.TRACE_LEVEL_INFO, "## CAMINHO: "+path);
				logger.writeln(ITraceInfo.TRACE_LEVEL_INFO, "## NOME DO ARQUIVO: "+arquivo);
				logger.writeln(ITraceInfo.TRACE_LEVEL_INFO, "###########################################################");
			} catch (IOException e) {
				logger.writeln(ITraceInfo.TRACE_LEVEL_INFO, "###########################################################");
				logger.writeln(ITraceInfo.TRACE_LEVEL_INFO, "ERRO:");
				e.printStackTrace();
				logger.writeln(ITraceInfo.TRACE_LEVEL_INFO, "###########################################################");
			}finally {
				try {
					if (fis != null) {
						fis.close();
					}
				} catch (IOException e) {
					e.getMessage();
				}
			}
			break;
			
		//arquivo para teste (Atualmente não tem nenhuma configuração)
		case CONFIG:
			arquivo = CONFIG;
			try {
				fis = new FileInputStream(path + arquivo);
				props = new Properties();
				props.load(fis);
				logger.writeln(ITraceInfo.TRACE_LEVEL_INFO, "######################### RJ ##############################");
				logger.writeln(ITraceInfo.TRACE_LEVEL_INFO, "## CAMINHO: "+path);
				logger.writeln(ITraceInfo.TRACE_LEVEL_INFO, "## NOME DO ARQUIVO: "+arquivo);
				logger.writeln(ITraceInfo.TRACE_LEVEL_INFO, "###########################################################");
			} catch (IOException e) {
				logger.writeln(ITraceInfo.TRACE_LEVEL_INFO, "###########################################################");
				logger.writeln(ITraceInfo.TRACE_LEVEL_INFO, "ERRO:");
				e.printStackTrace();
				logger.writeln(ITraceInfo.TRACE_LEVEL_INFO, "###########################################################");
			}finally {
				try {
					if (fis != null) {
						fis.close();
					}
				} catch (IOException e) {
					e.getMessage();
				}
			}
			break;
			
		default:
			System.out.println("Tipo de properties invalida");
			break;
		}
	}

	public String getArquivo() {
		return arquivo;
	}
	/**
	 * 
	 * @param String propriedade
	 * @return retorna o valor da propriedade
	 */
	public String getValor(String propriedade) {
		String valor = props.getProperty(propriedade);
		return valor;
	}

}
