package dasa.relatorio;

import com.avaya.sce.runtimecommon.ITraceInfo;

import dasa.util.Util;
import flow.IProjectVariables;

public class GravaPontoMarcacao {
	public void grava(String pontoMarcacao,com.avaya.sce.runtimecommon.SCESession mySession ) {
		ITraceInfo logger = mySession.getTraceOutput();

		logger.writeln(2,
				"####################################################");
		logger.writeln(2, "## PONTO: " + pontoMarcacao);

		String ucid = mySession.getVariableField(IProjectVariables.RELATORIO,IProjectVariables.RELATORIO_FIELD_UCID).getStringValue();

		mySession.getVariableField(IProjectVariables.RELATORIO,IProjectVariables.RELATORIO_FIELD_PONTO_MARCACAO).setValue(pontoMarcacao);

		mySession.getVariableField(IProjectVariables.RELATORIO,IProjectVariables.RELATORIO_FIELD_PONTO_RETENCAO).setValue(pontoMarcacao);

		pontoMarcacao = Util.concatenaValoresRelatorio(pontoMarcacao);

		logger.writeln(2, "## PONTO A GRAVAR: " + pontoMarcacao);

		GravarRelatorio.gravarPonto(pontoMarcacao, ucid);
		logger.writeln(2,"####################################################");
	}

}
