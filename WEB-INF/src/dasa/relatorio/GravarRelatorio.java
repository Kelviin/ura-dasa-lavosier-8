package dasa.relatorio;

import com.avaya.csi.sca.Call;
import com.avaya.csi.sca.CallException;

public class GravarRelatorio {
	public static boolean gravarPonto(String pontoMarcacao, String ucid) {
		boolean retorno = true;
		char primeiraLetra = pontoMarcacao.charAt(0);
		try {
			if(!Character.isLetter(primeiraLetra))
				Call.getInstance().storePoint(ucid, pontoMarcacao);
			else
				System.out.println("¨PONTO COM LETRA: " + pontoMarcacao);
		} catch (CallException e) {
			e.printStackTrace();
			retorno = false;
		}
		
		return retorno;
	}
	
	public static boolean gravarPontoOpcaoDinamico(String pontoMarcacao, String ucid, int opcao) {
		boolean retorno = true;
		char primeiraLetra = pontoMarcacao.charAt(0);
		
		try {
			if(!Character.isLetter(primeiraLetra))
				Call.getInstance().storePoint(ucid, pontoMarcacao);
			else
				System.out.println("¨PONTO COM LETRA: " + pontoMarcacao);
		} catch (CallException e) {
			e.printStackTrace();
			retorno = false;
		}
		
		return retorno;
	}
}
