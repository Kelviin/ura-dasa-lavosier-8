package flow;


import com.avaya.csi.sca.Call;
import com.avaya.csi.sca.CallException;
import com.avaya.sce.runtimecommon.ITraceInfo;

/**
 * A basic servlet which allows a user to define their code, generate
 * any output, and to select where to transition to next.
 * Last generated by Orchestration Designer at: 9 DE OUTUBRO DE 2018 9H39MIN21S BRT
 */
public class FinalizaWebReport extends com.avaya.sce.runtime.BasicServlet {

	//{{START:CLASS:FIELDS
	//}}END:CLASS:FIELDS

	/**
	 * Default constructor
	 * Last generated by Orchestration Designer at: 9 DE OUTUBRO DE 2018 9H39MIN21S BRT
	 */
	public FinalizaWebReport() {
		//{{START:CLASS:CONSTRUCTOR
		super();
		//}}END:CLASS:CONSTRUCTOR
	}

	/**
	 * This method allows for custom integration with other Java components.
	 * You may use Java for sophisticated logic or to integrate with custom
	 * connectors (i.e. JMS, custom web services, sockets, XML, JAXB, etc.)
	 *
	 * Any custom code added here should work as efficiently as possible to prevent delays.
	 * It's important to design your callflow so that the voice browser (Voice Portal/IR)
	 * is not waiting too long for a response as this can lead to a poor caller experience.
	 * Additionally, if the response to the client voice browser exceeds the configured
	 * timeout, the platform may throw an "error.badfetch". 
	 *
	 * Using this method, you have access to all session variables through the 
	 * SCESession object.
	 *
	 * The code generator will *** NOT *** overwrite this method in the future.
	 * Last generated by Orchestration Designer at: 9 DE OUTUBRO DE 2018 9H39MIN21S BRT
	 */
	public void servletImplementation(com.avaya.sce.runtimecommon.SCESession mySession) {

		ITraceInfo logger = mySession.getTraceOutput();
		
		try {
			
			String idMarca = mySession.getVariableField(IProjectVariables.VAR_APLICACAO,IProjectVariables.VAR_APLICACAO_FIELD_SIGLA_MARCA).getStringValue();
			String ucid = mySession.getVariableField(IProjectVariables.RELATORIO,IProjectVariables.RELATORIO_FIELD_UCID).getStringValue();
			String cpf = mySession.getVariableField(IProjectVariables.VAR_PACIENTE,IProjectVariables.VAR_PACIENTE_FIELD_CPF).getStringValue();
			String vdn = mySession.getVariableField(IProjectVariables.VAR_VDN, IProjectVariables.VAR_VDN_FIELD_VDN).getStringValue();
	
			//Store Info
			//Info 1 - Sigla Marca
			Call.getInstance().storeInfo(ucid, 1, "NewLA");
			
			//Info 2 - Localidade SP ou RJ
			Call.getInstance().storeInfo(ucid, 2, "SP");
			
			//Info 3 - CPF
			
			Call.getInstance().storeInfo(ucid, 3, cpf);
			
			//Info 4
//			idPaciente = mySession.getVariableField(IProjectVariables.VAR_IDENTIFICAR_PACIENTE,
//					IProjectVariables.VAR_IDENTIFICAR_PACIENTE_FIELD_ID_PACIENTE).getStringValue();
//			
//			Call.getInstance().storeInfo(ucid, 4, idPaciente);
//			
			
			//Info 5 - VDN de transferencia
			Call.getInstance().storeInfo(ucid, 5, vdn);
			
			logger.writeln(2, "## ------------------------------");
			logger.writeln(2, "## STORE POINTS");
			logger.writeln(2, "## STORE INFO 1: " + "NewLA");
			logger.writeln(2, "## STORE INFO 2: "  + "SP");
			logger.writeln(2, "## STORE INFO 3: " + cpf);
			logger.writeln(2, "## STORE INFO 4: " );
			logger.writeln(2, "## STORE INFO 5: " + vdn);
			logger.writeln(2, "## ------------------------------");
			logger.writeln(2, "## FINALIZA WEBREPORT");
			Call.getInstance().callFinish(ucid);
			logger.writeln(2, "## ------------------------------");
		} catch (CallException e) {
			logger.writeln(2, "## ERRO FINALIZA WEBREPORT");
			e.printStackTrace();
			
		}

	}
	/**
	 * Builds the list of branches that are defined for this servlet object.
	 * This list is built automatically by defining Goto nodes in the call flow editor.
	 * It is the programmer's responsibilty to provide at least one enabled Goto.<BR>
	 *
	 * The user should override updateBranches() to determine which Goto that the
	 * framework will activate.  If there is not at least one enabled Goto item, 
	 * the framework will throw a runtime exception.<BR>
	 *
	 * This method is generated automatically and changes to it may
	 * be overwritten next time code is generated.  To modify the list
	 * of branches for the flow item, override:
	 *     <code>updateBranches(Collection branches, SCESession mySession)</code>
	 *
	 * @return a Collection of <code>com.avaya.sce.runtime.Goto</code>
	 * objects that will be evaluated at runtime.  If there are no gotos
	 * defined in the Servlet node, then this returns null.
	 * Last generated by Orchestration Designer at: 31 DE MARÇO DE 2021 15H49MIN20S BRT
	 */
	public java.util.Collection getBranches(com.avaya.sce.runtimecommon.SCESession mySession) {
		java.util.List list = null;
		com.avaya.sce.runtime.Goto aGoto = null;
		list = new java.util.ArrayList(1);

		aGoto = new com.avaya.sce.runtime.Goto("Fim", 0, true, "Default");
		list.add(aGoto);

		return list;
	}
}
