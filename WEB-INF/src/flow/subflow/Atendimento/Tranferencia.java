package flow.subflow.Atendimento;

/**
 * This servlet is the exit point of a subflow.  The base class handles
 * the logic for forwarding to the next servlet.
 * Last generated by Orchestration Designer at: 4 DE OUTUBRO DE 2018 13H56MIN39S BRT
 */
public class Tranferencia extends com.avaya.sce.runtime.SubflowReturn {

	//{{START:CLASS:FIELDS
	//}}END:CLASS:FIELDS

	/**
	 * Default constructor
	 * Last generated by Orchestration Designer at: 4 DE OUTUBRO DE 2018 13H56MIN39S BRT
	 */
	public Tranferencia() {
		//{{START:CLASS:CONSTRUCTOR
		super();
		//}}END:CLASS:CONSTRUCTOR
	}

}
