package flow.subflow.Integracoes;

import java.util.ArrayList;
import java.util.List;

import br.com.coddera.receptivo.dasa.bean.ContatoBean;
import br.com.coddera.receptivo.dasa.bean.IdentificarPacienteBean;
import br.com.coddera.receptivo.dasa.bean.PacienteBean;
import br.com.coddera.receptivo.dasa.binding.SoapDataBinding;
import br.com.coddera.receptivo.dasa.business.IdentificarPacienteBusiness;

import com.avaya.sce.runtimecommon.ITraceInfo;

import dasa.util.Util;
import flow.IProjectVariables;

/**
 * A basic servlet which allows a user to define their code, generate
 * any output, and to select where to transition to next.
 * Last generated by Orchestration Designer at: 8 DE OUTUBRO DE 2018 16H38MIN3S BRT
 */
public class ConsultaANI extends com.avaya.sce.runtime.BasicServlet {

	//{{START:CLASS:FIELDS
	//}}END:CLASS:FIELDS

	/**
	 * Default constructor
	 * Last generated by Orchestration Designer at: 8 DE OUTUBRO DE 2018 16H38MIN3S BRT
	 */
	public ConsultaANI() {
		//{{START:CLASS:CONSTRUCTOR
		super();
		//}}END:CLASS:CONSTRUCTOR
	}

	/**
	 * This method allows for custom integration with other Java components.
	 * You may use Java for sophisticated logic or to integrate with custom
	 * connectors (i.e. JMS, custom web services, sockets, XML, JAXB, etc.)
	 *
	 * Any custom code added here should work as efficiently as possible to prevent delays.
	 * It's important to design your callflow so that the voice browser (Voice Portal/IR)
	 * is not waiting too long for a response as this can lead to a poor caller experience.
	 * Additionally, if the response to the client voice browser exceeds the configured
	 * timeout, the platform may throw an "error.badfetch". 
	 *
	 * Using this method, you have access to all session variables through the 
	 * SCESession object.
	 *
	 * The code generator will *** NOT *** overwrite this method in the future.
	 * Last generated by Orchestration Designer at: 8 DE OUTUBRO DE 2018 16H38MIN3S BRT
	 */
	public void servletImplementation(com.avaya.sce.runtimecommon.SCESession mySession) {

		ITraceInfo logger = mySession.getTraceOutput();
		mySession.getVariableField(IProjectVariables.VAR_CONSULTAS,IProjectVariables.VAR_CONSULTAS_FIELD_ERRO).setValue(0);
		mySession.getVariableField(IProjectVariables.VAR_INICIO,IProjectVariables.VAR_INICIO_FIELD_IDENTIFICOU_NUMERO_CHAMADOR).setValue(false);
		String siglaMarca = mySession.getVariableField(IProjectVariables.VAR_APLICACAO, IProjectVariables.VAR_APLICACAO_FIELD_SIGLA_MARCA).getStringValue();
		
		String telefone = mySession.getVariableField(IProjectVariables.SESSION, IProjectVariables.SESSION_FIELD_ANI).getStringValue();
		int tipoAplicacao = mySession.getVariableField(IProjectVariables.VAR_APLICACAO,IProjectVariables.VAR_APLICACAO_FIELD_TIPO_APLICACAO).getIntValue();
		
		String cpf = "";
		String dataNascimento = "";
		String convenio = "";
		String cep = "";
		
		boolean isPossuiCpf = false;
		boolean isVip = false;
		boolean isMedico = false;
		boolean isIdentificado = false;
		boolean isMenor = false;
		boolean isPossuiCEP = false;
		boolean isValidouContato = false;
		
		List<String> telefoneContatos = new ArrayList<>();
		
		logger.writeln(2, "########### Buscar Paciente por Telefone");
		logger.writeln(2, "##  TELEFONE : "+ telefone);
		logger.writeln(2, "##  MARCA : "+ mySession.getVariableField(IProjectVariables.VAR_APLICACAO_FIELD_MARCA).getStringValue());
		logger.writeln(2, "##  TIPO DE APLICAÇÃO: " + tipoAplicacao);
		logger.writeln(2, "##  SIGLA MARCA: " + siglaMarca);
		
		try {
						
		
			SoapDataBinding soapDataBinding = new SoapDataBinding();
			IdentificarPacienteBean identificarPacienteBean = new IdentificarPacienteBean();
			identificarPacienteBean.setPaciente(telefone);
			identificarPacienteBean.setMarca(siglaMarca);
			
			soapDataBinding.execute(identificarPacienteBean, new IdentificarPacienteBusiness(), tipoAplicacao);
			
			
			logger.writeln(3, "identificarPacienteBean.toString()" + identificarPacienteBean.toString());
			// RETORNO identificarPacienteBean.getResultado();
			// PACIENTE_NAO_ENCONTRADO (0),
			// PACIENTE_ENCONTRADO_EM_UM_CADASTRO (1),
			// PACIENTE_ENCONTRADO_EM_MAIS_DE_UM_CADASTRO (2),
			
			int resultado = identificarPacienteBean.getResultado();
			List<PacienteBean> listaPaciente = identificarPacienteBean.getPacientes();
						
			mySession.getVariableField(IProjectVariables.VAR_IDENTIFICA_PACIENTE,IProjectVariables.VAR_IDENTIFICA_PACIENTE_FIELD_LISTA_PACIENTES).setValue(listaPaciente);
						
			int quantidadePaciente = identificarPacienteBean.getPacientes().size();
			mySession.getVariableField(IProjectVariables.VAR_IDENTIFICA_PACIENTE,IProjectVariables.VAR_IDENTIFICA_PACIENTE_FIELD_QUANTIDADE_PACIENTES).setValue(quantidadePaciente);

			logger.writeln(2, "## QUANTIDADE PACIENTE: " + quantidadePaciente);
			
			if(resultado >0)
				mySession.getVariableField(IProjectVariables.VAR_INICIO,IProjectVariables.VAR_INICIO_FIELD_IDENTIFICOU_NUMERO_CHAMADOR).setValue(true);
			
			mySession.getVariableField(IProjectVariables.VAR_CONSULTAS,IProjectVariables.VAR_CONSULTAS_FIELD_RESULTADOS).setValue(resultado);
			
			if (quantidadePaciente == 1){
				
				mySession.getVariableField(IProjectVariables.VAR_IDENTIFICA_PACIENTE,IProjectVariables.VAR_IDENTIFICA_PACIENTE_FIELD_PACIENTE_OBJ).setValue(identificarPacienteBean.getPacientes().get(0));
				mySession.getVariableField(IProjectVariables.VAR_MENU_IDENTIFICADO, IProjectVariables.VAR_MENU_IDENTIFICADO_FIELD_CLIENTE_IDENTIFICADO).setValue(true);
				isIdentificado = (identificarPacienteBean.getResultado() > 0) ? true : false;
				
				if (isIdentificado)
					cep = identificarPacienteBean.getPacientes().get(0).getEndereco().getCep();
				
				logger.writeln(2, "## CEP: " + cep);
				if(!cep.isEmpty())
					isPossuiCEP = true;
				logger.writeln(2, "## NOME: " + identificarPacienteBean.getPacientes().get(0).getNome());
				logger.writeln(2, "## SEXO: " + identificarPacienteBean.getPacientes().get(0).getSexo());
				
				cpf = identificarPacienteBean.getPacientes().get(0).getCpf();
				if(cpf.length()==11){
					isPossuiCpf = true;
				}
				
				dataNascimento = identificarPacienteBean.getPacientes().get(0).getDataDeNascimento();
				
				logger.writeln(2, "## CPF: " + cpf);
				logger.writeln(2, "## TELEFONE: " + identificarPacienteBean.getPacientes().get(0).getContatos());
				logger.writeln(2, "## DATA NASCIMENTO: " + dataNascimento);
				
				mySession.getVariableField(IProjectVariables.VAR_PACIENTE, IProjectVariables.VAR_PACIENTE_FIELD_NOME).setValue(identificarPacienteBean.getPacientes().get(0).getNome());
				mySession.getVariableField(IProjectVariables.VAR_PACIENTE, IProjectVariables.VAR_PACIENTE_FIELD_SEXO).setValue(identificarPacienteBean.getPacientes().get(0).getSexo());
				mySession.getVariableField(IProjectVariables.VAR_PACIENTE, IProjectVariables.VAR_PACIENTE_FIELD_CPF).setValue(cpf);
				mySession.getVariableField(IProjectVariables.VAR_PACIENTE,IProjectVariables.VAR_PACIENTE_FIELD_CEP).setValue(cep);
				mySession.getVariableField(IProjectVariables.VAR_PACIENTE,IProjectVariables.VAR_PACIENTE_FIELD_POSSUI_CEP).setValue(isPossuiCEP);
				
				isMenor = identificarPacienteBean.getPacientes().get(0).isMenor();
				
				logger.writeln(2, "## MENOR: " + isMenor);
				
				mySession.getVariableField(IProjectVariables.VAR_PACIENTE,IProjectVariables.VAR_PACIENTE_FIELD_IS_MENOR).setValue(isMenor);
					
				//--------------------------------------
				//Verificando o Paciente é VIP
				//--------------------------------------
				isVip = identificarPacienteBean.getPacientes().get(0).isVip();
				logger.writeln(2, "## VIP: " + isVip);
				mySession.getVariableField(IProjectVariables.VAR_TRIAGEM, IProjectVariables.VAR_TRIAGEM_FIELD_EH_PACIENTE_VIP).setValue(isVip);
				
				//--------------------------------------
				//Verificando o Paciente é Médico
				//--------------------------------------
				isMedico = identificarPacienteBean.getPacientes().get(0).isMedico();
				logger.writeln(2, "## MEDICO: " + isMedico);
				mySession.getVariableField(IProjectVariables.VAR_TRIAGEM,IProjectVariables.VAR_TRIAGEM_FIELD_EH_MEDICO).setValue(isMedico);
				
				//Verficia se paciente e VIP ou Medico caso seja é PACIENTE CONCIERGE
				if(isVip || isMedico){
					mySession.getVariableField(IProjectVariables.VAR_INICIO,IProjectVariables.VAR_INICIO_FIELD_DDRS_1_1_2_5_4_0_6_6_7_8OU_2_1_2_2_2_7_8_0_3_0).setValue(true);
				}
				//----------------------
				
				//--------------------------------------
				//Verificando o convenio do Paciente
				//--------------------------------------
				convenio = identificarPacienteBean.getPacientes().get(0).getPlano().getConvenio();
				if("IAMSPE".equalsIgnoreCase(convenio) || "IAMSPE#".equalsIgnoreCase(convenio) || "IAMSPEACDA".equalsIgnoreCase(convenio) || "IAMSPEACLV".equalsIgnoreCase(convenio)){
					logger.writeln(2, "## CONVENIO: " + convenio);
					mySession.getVariableField(IProjectVariables.VAR_PACIENTE, IProjectVariables.VAR_PACIENTE_FIELD_CONVENIO).setValue(0);
				}else {
					if (!convenio.isEmpty()){
						logger.writeln(2, "## CONVENIO: " + convenio);
						mySession.getVariableField(IProjectVariables.VAR_PACIENTE, IProjectVariables.VAR_PACIENTE_FIELD_CONVENIO).setValue(1);
					} else {
						logger.writeln(2, "## CONVENIO NÃO IDENTIFICADO");
						mySession.getVariableField(IProjectVariables.VAR_PACIENTE, IProjectVariables.VAR_PACIENTE_FIELD_CONVENIO).setValue(1);

					}
				}
				
				if (identificarPacienteBean.getPacientes().get(0).getContatos().size() > 0){
					for (ContatoBean contatoBean : identificarPacienteBean.getPacientes().get(0).getContatos()) {
						telefoneContatos.add(contatoBean.getContato());
						logger.writeln(2, "## CONTATOS: " + contatoBean.getContato());
						isValidouContato = true;
					}
				}
				
			}
				
			if(!(identificarPacienteBean.getPacientes().isEmpty())  &&  identificarPacienteBean.getPacientes() != null && identificarPacienteBean.getPacientes().size() > 0){
				if (identificarPacienteBean.getPacientes().size() == 1) {
					mySession.getVariableField(IProjectVariables.VAR_PACIENTE,IProjectVariables.VAR_PACIENTE_FIELD_ID_PACIENTE).setValue(identificarPacienteBean.getPacientes().get(0).getIdPaciente());
					logger.writeln(2, "## IDPACIENTE: " + identificarPacienteBean.getPacientes().get(0).getIdPaciente());
				}else{
					logger.writeln(2, "## ENCONTOU MAIS DE 1 IDPACIENTE: ");
					for (int i = 0; i < identificarPacienteBean.getPacientes().size(); i++) {
						logger.writeln(2, "## ID"+i+": "+identificarPacienteBean.getPacientes().get(i).getIdPaciente());
					}
				}
			}else{
				logger.writeln(2, "## NAO FOI ENCONTRADO NENHUM ID");
			}
			
			mySession.getVariableField(IProjectVariables.VAR_CONSULTAS,IProjectVariables.VAR_CONSULTAS_FIELD_ERRO).setValue(0);
			
			logger.writeln(2, "#############################################");
		} catch (Exception e) {			
			logger.writeln(2, "## Erro");
			logger.writeln(2, "## "+ e.getMessage());
			mySession.getVariableField(IProjectVariables.VAR_CONSULTAS,IProjectVariables.VAR_CONSULTAS_FIELD_ERRO).setValue(Util.verificaRetornoErro(e.getCause()));
			logger.writeln(2, "#############################################");
		}
			
		
	}
	/**
	 * Builds the list of branches that are defined for this servlet object.
	 * This list is built automatically by defining Goto nodes in the call flow editor.
	 * It is the programmer's responsibilty to provide at least one enabled Goto.<BR>
	 *
	 * The user should override updateBranches() to determine which Goto that the
	 * framework will activate.  If there is not at least one enabled Goto item, 
	 * the framework will throw a runtime exception.<BR>
	 *
	 * This method is generated automatically and changes to it may
	 * be overwritten next time code is generated.  To modify the list
	 * of branches for the flow item, override:
	 *     <code>updateBranches(Collection branches, SCESession mySession)</code>
	 *
	 * @return a Collection of <code>com.avaya.sce.runtime.Goto</code>
	 * objects that will be evaluated at runtime.  If there are no gotos
	 * defined in the Servlet node, then this returns null.
	 * Last generated by Orchestration Designer at: 30 DE MARÇO DE 2021 16H52MIN27S BRT
	 */
	public java.util.Collection getBranches(com.avaya.sce.runtimecommon.SCESession mySession) {
		java.util.List list = null;
		com.avaya.sce.runtime.Goto aGoto = null;
		list = new java.util.ArrayList(1);

		aGoto = new com.avaya.sce.runtime.Goto("Integracoes-VerificaSaida", 0, true, "Default");
		list.add(aGoto);

		return list;
	}
}
