package flow.subflow.Identificacao;

/**
 * This servlet is the exit point of a subflow.  The base class handles
 * the logic for forwarding to the next servlet.
 * Last generated by Orchestration Designer at: 28 DE MAIO DE 2020 10H41MIN8S BRT
 */
public class Desconexao extends com.avaya.sce.runtime.SubflowReturn {

	//{{START:CLASS:FIELDS
	//}}END:CLASS:FIELDS

	/**
	 * Default constructor
	 * Last generated by Orchestration Designer at: 28 DE MAIO DE 2020 10H41MIN8S BRT
	 */
	public Desconexao() {
		//{{START:CLASS:CONSTRUCTOR
		super();
		//}}END:CLASS:CONSTRUCTOR
	}

}
