package flow;

/**
 * This servlet is used to forward the request to the entry point of a
 * project callflow (subflow).
 * Last generated by Orchestration Designer at: 9 DE OUTUBRO DE 2018 9H45MIN21S BRT
 */
public class Encerramento extends com.avaya.sce.runtime.Subflow {

	//{{START:CLASS:FIELDS
	//}}END:CLASS:FIELDS

	/**
	 * Default constructor
	 * Last generated by Orchestration Designer at: 9 DE OUTUBRO DE 2018 9H45MIN21S BRT
	 */
	public Encerramento() {
		//{{START:CLASS:CONSTRUCTOR
		super();
		//}}END:CLASS:CONSTRUCTOR
	}

	/**
	 * Returns the name of the subflow that is being invoked.  This name is used for
	 * determining the URL mapping for the the entry point of the subflow..
	 *
	 * @return the name of the subflow
	 * Last generated by Orchestration Designer at: 31 DE MARÇO DE 2021 15H49MIN20S BRT
	 */
	protected String getSubflowName() {
		return("Encerramento");
	}
	/**
	 * Returns the name of the mapping of sub flow exit points to the URL mappings
	 * of the servlets to return back to in the calling flow.
	 *
	 * @return map of sub flow exit points to servlets in the calling flow.
	 * Last generated by Orchestration Designer at: 31 DE MARÇO DE 2021 15H49MIN20S BRT
	 */
	protected java.util.Map<String,String> getExitPoints() {
		java.util.Map<String, String> exitPoints;
		exitPoints = new java.util.HashMap<String, String>();
		exitPoints.put("Encerramento-Desconexao", "FinalizaWebReport");
		exitPoints.put("Encerramento-Abandono", "FinalizaWebReport");
		exitPoints.put("Encerramento-Atendimento", "Atendimento");
		return exitPoints;
	}
}
